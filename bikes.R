setwd("C:/���/�������/��� �/����� ���/datafiles")
bike.data.raw <- read.csv("bike sharing.csv")

str(bikes.data.raw)
library(ggplot2)

bikes.data.prepared <- bikes.data.raw

# turn date time into caharcter 
bikes.data.prepared$datetime <- as.character(bikes.data.prepared$datetime)
str(bikes.data.prepared)


Sys.setlocale("LC_TIME", "English")
date <- substr(bikes.data.prepared$datetime, 1,10)
days <- weekdays(as.Date(date))

#Add weekdays 
bikes.data.prepared$weekday <- as.factor(days)

str(bikes.data.prepared)

#Add month 
month <- as.integer(substr(date, 6,7))
bikes.data.prepared$month <- month

#Add year 
year <- as.integer(substr(date, 1,4))
bikes.data.prepared$year <- year

hour <- as.integer(substr(bikes.data.prepared$datetime, 12,13))
bikes.data.prepared$hour <- hour

#Find seasons 
table(bikes.data.prepared$season,bikes.data.prepared$month)

#Turn season into factor 
bikes.data.prepared$season <- factor(bikes.data.prepared$season, levels = 1:4, labels = c('winter',
                                                                                          'spring', 'summer','automn'))



#home work solution 

#Turn weather into a factor 
bikes.data.prepared$weather <- factor(bikes.data.raw$weather, levels=1:4, labels = c( "Clear", "Cloudy", "Light Rain", "Heavy Rain"))

#Show how weather affects count 
ggplot(bikes.data.prepared, aes(weather,count)) + geom_boxplot()

#Show how season affects count 
ggplot(bikes.data.prepared, aes(season ,count)) + geom_boxplot()

str(bikes.data.prepared)

#Look at time impact 
ggplot(bikes.data.prepared, aes(as.factor(year), count)) + geom_boxplot()
ggplot(bikes.data.prepared, aes(as.factor(month), count)) + geom_boxplot()
ggplot(bikes.data.prepared, aes(day, count)) + geom_boxplot()
ggplot(bikes.data.prepared, aes(as.factor(hour), count)) + geom_boxplot()

#binning hour 
breaks <- c(0,7,8,9,17,20,21,22,24)
labels <- c("0-6", "7", "8", "9-16", "17-19", "20", "21", "22-23")
bins <- cut(bikes.data.prepared$hour, breaks, include.lowest = T, right = F,labels = labels)

#Add to dataframe 
bikes.data.prepared$hourb <- bins
str(bikes.data.prepared)
ggplot(bikes.data.prepared, aes(hourb, count)) + geom_boxplot()

#How temperature affects count 
ggplot(bikes.data.prepared, aes(temp, count)) + geom_point() + stat_smooth(method = lm)

#How wind affects count 
ggplot(bikes.data.prepared, aes(windspeed, count)) + geom_point() + stat_smooth(method = lm)

#How wind affects count 
ggplot(bikes.data.prepared, aes(humidity, count)) + geom_point() + stat_smooth(method = lm)



str(bikes.data.prepared)

bikes.data.final <- bikes.data.prepared  

bikes.data.final$datetime <- NULL
bikes.data.final$season <- NULL
bikes.data.final$atemp <- NULL
bikes.data.final$casual <- NULL
bikes.data.final$registered <- NULL
bikes.data.final$hour <- NULL

str(bikes.data.final)

#install.packages("caTools")
library(caTools)

filter <- sample.split(bikes.data.final$count, SplitRatio = 0.7)

#Training set 
bikes.train <- subset(bikes.data.final, filter==TRUE)

#test set 
bikes.test <- subset(bikes.data.final, filter==F)

dim(bikes.data.final)
dim(bikes.train)
dim(bikes.test)

model <- lm(count ~ .,bikes.train )

summary(model)

predict.train <- predict(model,bikes.train)
ptrdict.test  <- predict(model,bikes.test)

#install.packages('lubridate')
library(lubridate)

#see prediction per day 
predicted.all <- predict(model,bikes.data.final)

bikes.analysis <- bikes.data.final

bikes.analysis$predicted <- predicted.all

datetime <- bikes.data.prepared$datetime

bikes.analysis$date <- as.Date(datetime) 

bikes.analysis$month.year <- floor_date(bikes.analysis$date, "month") 

library(dplyr)

bikes.month <- bikes.analysis %>% group_by(month.year) %>%
  summarize(actual=sum(count), predicted = sum(predicted))

ggplot() + 
  geom_line(data = bikes.month, aes(month.year, actual), color = 'red' ) +
  geom_line(data = bikes.month, aes(month.year, predicted), color = 'blue') 






