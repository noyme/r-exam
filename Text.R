setwd("C:/���/�������/��� �/����� ���/datafiles")

spam.raw <- read.csv('spam.csv', stringsAsFactors = FALSE)

str(spam.raw)
spam <- spam.raw

spam$type <-as.factor(spam$type)
str(spam)

library(ggplot2)

ggplot(spam , aes(spam$type))+geom_bar()

head(spam)

#install.packages('tm')
library(tm)

spam.corpus <- Corpus(VectorSource(spam$text))
clean.corpus <- tm_map(spam.corpus, removePunctuation)
clean.corpus[[1]][[1]]

clean.corpus <- tm_map(clean.corpus, removeNumbers)

clean.corpus <- tm_map(clean.corpus, content_transformer(tolower))

clean.corpus <- tm_map(clean.corpus, removeWords, stopwords())

clean.corpus <- tm_map(clean.corpus, stripWhitespace)

dtm <- DocumentTermMatrix(clean.corpus)
dim(dtm)

dtm.freq <- DocumentTermMatrix(clean.corpus, list(dictionary = findFreqTerms(dtm,10)))
dim(dtm.freq)

inspect(dtm.freq[1:10,1:20])

conv_01 <- function(x){
  x<- ifelse(x>0,1,0)
  return(as.integer(x))
}
dtm.final <- apply(dtm.freq, MARGIN = 1:2, conv_01)
inspect(dtm.final)

dtm.df <- as.data.frame(dtm.final)

conv_01_type <- function(x){
  if (x=='ham') return(as.integer(0))
  return(as.integer(1))
  
}
spam$type <- sapply(spam$type,conv_01_type)

dtm.df$type <- spam$type
str(dtm.df)

#Ex9
library(caTools)

filter <- sample.split(dtm.df$type, SplitRatio = 0.7)

spam.train = subset(dtm.df, filter == T)
spam.test = subset(dtm.df, filter == F)
dim(spam.test)
dim(spam.train)

spam.model <- glm(type ~., family = binomial(link = 'logit'), data = spam.train)
summary(spam.model)

prediction <- predict(spam.model,spam.test , type = 'response')

hist(prediction)

actual <- spam.test$type

confusion_matrix <- table(actual, prediction>0.5)

precision <- confusion_matrix[2,2]/ (confusion_matrix[2,2]+confusion_matrix[1,2])
recall <- confusion_matrix[2,2]/ (confusion_matrix[2,2]+confusion_matrix[2,1])

#install.packages('ROSE')
library(ROSE)

spam.train.over <- ovun.sample(type ~., data = spam.train, method = 'over', N = 600)$data

table(spam.train.over$type)

spam.model.over <- glm(type ~., family = binomial(link = 'logit'), data = spam.train.over)
summary(spam.model)

prediction.over <- predict(spam.model.over,spam.test , type = 'response')

hist(prediction)

actual <- spam.test$type

confusion_matrix_over <- table(actual, prediction.over >0.5)

precision.over <- confusion_matrix_over[2,2]/ (confusion_matrix_over[2,2]+confusion_matrix_over[1,2])
recall.over <- confusion_matrix_over[2,2]/ (confusion_matrix_over[2,2]+confusion_matrix_over[2,1])
