#install.packages('ggplot2')
#install.packages('dplyr')
library(ggplot2)
library(dplyr)

setwd("C:/���/�������/��� �/����� ���/datafiles")

#data frame
#jhdfh
df.raw <- read.csv(url('https://raw.githubusercontent.com/datasets/covid-19/master/data/countries-aggregated.csv'))

str(df)

df<- df.raw


df$Date <- as.Date(df.raw$Date)

str(df)

df$Active <-df$Confirmed - df$Recovered - df$Deaths

df$Mort.rate <- df$Deaths/(df$Confirmed+1)

df.israel <- df %>% filter(Country == 'Israel')

ggplot(df.israel, aes(Date,Confirmed)) + geom_point()
ggplot(df.israel , aes(Date , Active)) + geom_point()
ggplot(df.israel , aes(Date , Recovered)) + geom_point()
ggplot(df.israel , aes(Date , Deaths)) + geom_point()
ggplot(df.israel , aes(Date , Mort.rate)) + geom_point()

lag <- function(date, country, df, days){
  day <- date - days
  if(day >= df$Date[1]){
    v <- df %>% filter(Date == day , Country == country)
    return(v$Confirmed)
  }
  else{
    return(0)
  }
}

confirmed.lag <- mapply(lag, df$Date, df$Country, MoreArgs = list(df,5))

df$Confirmed.lag <- confirmed.lag
df$confirmed.lag <- NULL  #delet colum

df$Mort.rate.fixed <- df$Deaths/(df$Confirmed.lag+1)


df.israel <- df %>% filter(Country == 'Israel')

ggplot() +
  geom_point(data = df.israel, aes(Date,Mort.rate.fixed), color = 'red') +
  geom_point(data = df.israel, aes(Date,Mort.rate), color = 'blue')

df.sweden <- df %>% filter (Country == 'Sweden')

ggplot() + 
  #israel
  geom_point(data = df.israel , aes(Date , Confirmed),color =  'red') + 
  #Sweden
  geom_point(data = df.sweden , aes(Date , Confirmed),color = 'blue')
df.sweaden = NULL

ggplot() + 
  #israel
  geom_point(data = df.israel , aes(Date , Deaths),color =  'red') + 
  #Sweden
  geom_point(data = df.sweden , aes(Date , Deaths),color = 'blue')

ggplot() + 
  #israel
  geom_point(data = df.israel , aes(Date , Confirmed),color =  'red') + 
  #Sweden
  geom_point(data = df.israel , aes(Date , Recovered),color = 'blue')

#build column chart for mort rate

countries <- df %>% distinct(Country)

interesting.countries <- c('Belgium','Denmark','US','Sweden','France','Germany','Greece', 'Israel','Italy','Japan','Switzerland','United Kingdom')

df.int.countries <- df %>% filter(Country %in% interesting.countries)

today <- as.Date('2020-05-01')

df.int.countries.today <- df.int.countries %>% filter (Date == today)

str(df.int.countries.today)

ggplot (df.int.countries.today, aes(reorder(Country , -Mort.rate.fixed), Mort.rate.fixed)) + geom_col()

write.csv(df.int.countries.today, file = 'Corona-01-05.csv')
countries.age <- read.csv('Corona-01-05.csv')

str(countries.age)

ggplot(countries.age , aes(age, Mort.rate.fixed)) + geom_point() +
  geom_text(aes(label = Country) , hjust = 0 , vjust = 0)

df.aggragate <- df %>%
                group_by(Date) %>%
                summarize(total = sum(Confirmed))

day.diff <- function(date , df){
  today.df <- df %>% filter( Date == date)
  today <- today.df$total
  yesterday.df <- df %>% filter(Date == date-1)
  if (nrow(yesterday.df)==0) return(0)
  yesterday <- yesterday.df$total
  return(today - yesterday)
}

diff <- sapply(df.aggragate$Date ,day.diff, df = df.aggragate)

df.aggragate$diff <- diff

ggplot (df.aggragate , aes(Date, diff)) + geom_area()


countries.vec <- c('Vietnam', 'Turkey', 'Iran', 'Germany', 'Thailand', 'France', 'United Kingdom', 'Italy', 'Spain', 'Colombia')


df.countries <- df %>% filter(Country %in% countries.vec)

today1 <- as.Date('2020-05-07')
df.countries.today <- df.countries %>% filter (Date == today)

write.csv(df.countries.today, file = 'Corona-Ex.csv')
countries.deatails <- read.csv('Corona-Ex5.csv')

ggplot(countries.deatails, aes(x=ave.latitude, y=Mort.rate.fixed, size = madian.age)) +
  geom_point(alpha=0.5)